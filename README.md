# Portfolio d'Aurélien Abrassart

Bienvenue sur le dépôt du portfolio d'[Aurélien Abrassart](https://abrassart.dev/). Ce portfolio est conçu pour mettre en valeur mes compétences et mes projets en tant que développeur Front-End spécialisé en **ReactJS** et **Next.js**.

## 🚀 Technologies utilisées

Ce projet est construit avec les technologies suivantes :

- [Next.js](https://nextjs.org/) - Framework React pour le rendu côté serveur et le développement rapide d'applications web modernes.
- [React](https://reactjs.org/) - Bibliothèque JavaScript pour créer des interfaces utilisateur.
- [CSS Modules](https://github.com/css-modules/css-modules) - Gestion des styles au niveau des composants.
- [Vercel](https://vercel.com/) - Hébergement performant et déploiement continu.

---

## 🌐 Accéder au portfolio

Explorez le portfolio ici : [abrassart.dev](https://abrassart.dev/).

---

## 🔧 Installation et configuration du projet

Pour cloner et exécuter ce projet localement, suivez les étapes ci-dessous :

### 1. Prérequis

Assurez-vous que votre environnement dispose des éléments suivants :

- [Node.js](https://nodejs.org/) version 14 ou supérieure
- [npm](https://www.npmjs.com/) ou [yarn](https://yarnpkg.com/)
