import { Outfit } from "next/font/google";
import "./globals.css";
import Header from "@/components/Header";

const outfit = Outfit({ subsets: ["latin"] });

export const metadata = {
  title: "Abrassart's Portfolio",
  description: "Abrassart's Portfolio",
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <head>
        <link rel="icon" href="/favicon.ico" />
      </head>
      <body className={outfit.className}>
        <Header />
        {children}
      </body>
    </html>
  );
}
