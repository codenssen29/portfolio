"use client";
import React from "react";
import Hero from "@/components/Hero";
import SkillIcon from "@/components/SkillIcon";
import Projet from "@/components/Projet";
import Footer from "@/components/Footer";

const projets = [
  {
    name: "Optivous.fr",
    subtitle: "Application de prise de rendez-vous à domicile",
    imgsrc: "bg-barber",
    description:
      "L'application offre aux entrepreneurs à domicile la possibilité de mettre en place un système de réservation pour leurs clients, permettant une optimisation des trajets en fonction des rendez-vous fixés. J'ai utilisé une stack technique comprenant ReactJS, Tailwind CSS, NodeJS/Express, MySQL et Docker pour le développement.",
    buttons: [
      {
        url: "https://gitlab.com/codenssen29/cda_projet_frontend",
        name: "Dépôt front-end",
        type: "git",
      },
      {
        url: "https://gitlab.com/codylab/cda_projet",
        name: "Dépôt back-end",
        type: "git",
      },
      {
        url: "https://optivous.fr",
        name: "Lien du projet",
        type: "link",
      },
    ],
    stack: ["javascript", "react", "tailwind", "nodejs", "mysql", "docker"],
  },
  {
    name: "Service d'acquisition de prospects",
    subtitle: "En cours de développement",
    imgsrc: "bg-company",
    description:
      "L’application vise à collecter un maximum d’informations sur les entreprises nouvellement créées grâce aux données ouvertes disponibles. Elle permet de trier et d’afficher ces entreprises tout en envoyant des emails et/ou des courriers physiques automatiques. J’ai utilisé une pile technologique comprenant ReactJS, TailwindCSS, NodeJS/Express, MongoDB, Docker et déploiement avec GitLab CI/CD.",
    buttons: [],
    stack: ["javascript", "react", "tailwind", "nodejs", "mongodb", "docker"],
  },
  {
    name: "Portfolio",
    subtitle: "NextJS Portfolio",
    imgsrc: "bg-portfolio",
    description:
      "Mon portfolio développé avec Next.js a pour objectif de proposer une interface ergonomique, esthétique et fonctionnelle. L'intérêt de Next.js réside dans sa capacité à générer des pages côté serveur, optimisant ainsi le référencement naturel (SEO). Le portfolio est déployé automatiquement sur Vercel et utilise un nom de domaine personnalisé.",
    buttons: [
      {
        url: "https://gitlab.com/codenssen29/portfolio",
        name: "Dépôt portfolio",
        type: "git",
      },
    ],
    stack: ["javascript", "nextjs", "tailwind", "vercel"],
  },
];

const skills = [
  {
    logo: "javascript",
    name: "Javascript",
  },

  {
    logo: "html",
    name: "HTML/CSS",
  },
  {
    logo: "react",
    name: "ReactJS",
    url: "https://react.dev/",
  },
  {
    logo: "nextjs",
    name: "NextJS",
    url: "https://nextjs.org/",
  },
  {
    logo: "nodejs",
    name: "NodeJS",
    url: "https://nodejs.org/en",
  },
  {
    logo: "bootstrap",
    name: "BootStrap",
    url: "https://getbootstrap.com/",
  },
  {
    logo: "tailwind",
    name: "Tailwind CSS",
    url: "https://tailwindcss.com/",
  },
  {
    logo: "jest",
    name: "Jest",
    url: "https://jestjs.io/",
  },
  {
    logo: "php",
    name: "PHP",
  },
  {
    logo: "csharp",
    name: "C# ASP.NET",
    url: "https://learn.microsoft.com/fr-fr/dotnet/csharp/tour-of-csharp/",
  },
  {
    logo: "entity",
    name: ".NET Entity",
    url: "https://learn.microsoft.com/fr-fr/ef/",
  },
  {
    logo: "mysql",
    name: "MySQL",
  },
  {
    logo: "mongodb",
    name: "MongoDB",
  },
  {
    logo: "postman",
    name: "Postman",
    url: "https://www.postman.com/",
  },
  {
    logo: "figma",
    name: "Figma",
    url: "https://www.figma.com/",
  },
  {
    logo: "docker",
    name: "Docker",
    url: "https://www.docker.com/",
  },
  {
    logo: "pm2",
    name: "PM2",
    url: "https://pm2.keymetrics.io/",
  },
  {
    logo: "seo",
    name: "SEO/SEA",
    url: "https://www.francenum.gouv.fr/guides-et-conseils/communication-et-publicite/publicite-sur-internet/referencement-seo-vs-publicite",
  },
  {
    logo: "wordpress",
    name: "WordPress",
    url: "https://wordpress.org/",
  },
];

export default function Home() {
  return (
    <main>
      <Hero />
      <div className="items-center pb-3  justify-center flex mt-10 border-b-2 border-orange-200">
        <h1
          className="text-5xl text-center font-bold text-orange-600"
          id="skills"
        >
          Mes compétences
        </h1>
      </div>
      <div className="flex justify-center">
        <div className="w-full flex sm:flex-row flex-wrap gap-5 items-stretch justify-center my-10">
          {skills.map((skill) => (
            <SkillIcon
              key={skill.id}
              logo={skill.logo}
              name={skill.name}
              description={skill.description}
              url={skill.url}
            />
          ))}
        </div>
      </div>
      <div className="items-center pb-3 justify-center flex mt-10 border-b-2 border-orange-200">
        <h1
          className="text-5xl text-center font-bold text-orange-600"
          id="projects"
        >
          Mes projets
        </h1>
      </div>
      <div className="mt-4 mb-8 flex flex-col gap-12 py-10">
        {projets.map((projet) => {
          return <Projet key={projet.id} projet={projet} />;
        })}
      </div>
      <Footer />
    </main>
  );
}
