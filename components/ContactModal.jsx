import React from "react";

const ContactModal = ({ isOpen, onClose }) => {
  return (
    <div className={`modal ${isOpen ? "open" : ""}`}>
      {/* Contenu de la fenêtre modale (formulaire de contact, etc.) */}
      <button onClick={onClose}>Fermer</button>
    </div>
  );
};

export default ContactModal;
