export default function Footer() {
  return (
    <div className="w-full h-7 flex items-center justify-center pt-12">
      <span className="font-light">
        ABRASSART Aurélien - Tous droits réservés 2024 -
      </span>
    </div>
  );
}
