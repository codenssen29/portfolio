"use client";

import { useEffect, useState } from "react";
import Nav from "./Nav";
import Link from "next/link";

export default function Header() {
  const [header, SetHeader] = useState(false);

  const scrollHeader = () => {
    if (window.scrollY >= 50) {
      SetHeader(true);
    } else {
      SetHeader(false);
    }
  };

  useEffect(() => {
    window.addEventListener("scroll", scrollHeader);

    return () => {
      window.addEventListener("scroll", scrollHeader);
    };
  });

  const links = [
    {
      name: "Compétences",
      url: "/#skills",
    },
    {
      name: "Projets",
      url: "/#projects",
    },
  ];

  return (
    <div
      onMouseEnter={() => SetHeader(false)}
      onMouseLeave={() => {
        if (window.scrollY >= 50) {
          SetHeader(true);
        }
      }}
      className={
        header
          ? "z-50 fixed top-0 w-full bg-orange-700 opacity-70 duration-500"
          : "z-50 fixed top-0 w-full bg-orange-700 duration-500"
      }
    >
      <div
        className={
          header
            ? "w-full h-16 sm:h-14 transition-height duration-500 flex-row sm:flex-row flex items-center justify-between py-4 px-4 sm:px-20"
            : "w-full h-52 sm:h-20 transition-height duration-500 flex-col sm:flex-row flex items-center justify-between py-4 sm:px-20"
        }
      >
        <a
          href="/"
          className="group text-orange-100 transition-all duration-300 ease-in-out"
        >
          <span className="bg-left-bottom bg-gradient-to-r from-orange-700 to-orange-200 bg-[length:0%_1px] bg-no-repeat group-hover:bg-[length:100%_1px] transition-all duration-200 ease-out font-normal text-xl">
            Aurélien ABRASSART
          </span>
        </a>

        <div
          className={
            header ? "hidden sm:block w-full sm:w-1/2" : "w-full sm:w-1/2"
          }
        >
          <Nav links={links} />
        </div>
        <Link
          className={header ? "hidden white-button" : "white-button"}
          href="mailto:aurelien@codycom.fr?subject=ABRASSART.dev - Prise de contact"
        >
          Me contacter
          <svg
            className="rtl:rotate-180 w-3.5 h-3.5 ms-2"
            aria-hidden="true"
            xmlns="http://www.w3.org/2000/svg"
            fill="none"
            viewBox="0 0 14 10"
          >
            <path
              stroke="currentColor"
              strokeLinecap="round"
              strokeLinejoin="round"
              strokeWidth="2"
              d="M1 5h12m0 0L9 1m4 4L9 9"
            />
          </svg>
        </Link>
      </div>
    </div>
  );
}
