import Image from "next/image";
import Link from "next/link";
import Linkedin from "../img/linkedin.svg";
import Gitlab from "../img/gitlab.svg";
import Xlogo from "../img/x.svg";
import CodycomLogo from "../img/codycom-logo.png";
import ProfilPhoto from "../img/photo-profil-aurelien.png";

const socialLinks = [
  {
    href: "https://www.linkedin.com/in/aurelien-abrassart/",
    src: Linkedin,
    alt: "Linkedin Logo",
  },
  {
    href: "https://gitlab.com/codenssen29",
    src: Gitlab,
    alt: "Gitlab Logo",
  },
  {
    href: "https://x.com/codenssen",
    src: Xlogo,
    alt: "X Twitter Logo",
  },
  {
    href: "https://codycom.fr/",
    src: CodycomLogo,
    alt: "Codycom logo",
  },
];

const HeroButton = ({ url, children, target }) => {
  return (
    <Link className="default-button" href={url} target={target}>
      {children}
      <svg
        className="rtl:rotate-180 w-3.5 h-3.5 ms-2"
        aria-hidden="true"
        xmlns="http://www.w3.org/2000/svg"
        fill="none"
        viewBox="0 0 14 10"
      >
        <path
          stroke="currentColor"
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth="2"
          d="M1 5h12m0 0L9 1m4 4L9 9"
        />
      </svg>
    </Link>
  );
};

const SocialLinks = () => {
  return (
    <div className="flex flex-row gap-4">
      {socialLinks.map((link) => (
        <Link key={link.href} href={link.href} target="_blank">
          <Image
            src={link.src}
            height={40}
            alt={link.alt}
            className="hover:-translate-y-1 duration-150"
          />
        </Link>
      ))}
    </div>
  );
};

export default function Hero() {
  return (
    <div className="h-max flex flex-col lg:flex-row bg-[radial-gradient(ellipse_at_top,_var(--tw-gradient-stops))] from-orange-100 via-orange-50 to-white">
      <div className="w-full lg:w-1/2 p-10 lg:p-28 flex flex-col">
        <SocialLinks />
        <span className="mt-10 text-5xl font-bold">🖐️ Salut !</span>
        <span className="text-5xl font-bold bg-left-bottom bg-gradient-to-r from-orange-300 to-orange-100 bg-[length:400px_20px] bg-no-repeat">
          C&apos;est Aurélien ABRASSART.
        </span>
        <h2 className="text-4xl">Développeur Web</h2>
        <span className="mt-4 w-3/4">
          Bienvenue sur mon portfolio. Vous trouverez sur ce site mes
          compétences, ainsi que mes projets. N&apos;hésitez pas à me contacter
          pour en savoir plus !
        </span>
        <div className="mt-10 flex flex-col sm:flex-row items-center gap-10 md:flex-wrap">
          <HeroButton url="/#skills">Mes compétences</HeroButton>
          <HeroButton url="/#projects">Mes projets</HeroButton>
          <HeroButton url="/CV_ABRASSART_web.pdf" target="_blank">
            Télécharger mon CV
          </HeroButton>
        </div>
      </div>
      <div className="lg:w-1/2 p-10 flex flex-row items-center justify-center lg:bg-hero bg-cover rounded-tl-full">
        <Image
          src={ProfilPhoto}
          alt="Profil Photo"
          className="w-56 rounded-r-full rounded-tl-full"
          quality={80}
        />
      </div>
    </div>
  );
}
