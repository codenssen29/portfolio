import Link from "next/link";

export default function Nav({ links }) {
  return (
    <nav>
      <ul className="list-none flex w-full flex-row items-center justify-evenly">
        {/* <li className="p-3">
        <Link href="/">Accueil</Link>
      </li>
      <li className="p-3">
        <Link href="/projets">Projets</Link>
      </li> */}
        {links &&
          links.map((link) => (
            <li
              key={link.name}
              className="group text-orange-100 transition-all duration-300 ease-in-out"
            >
              <Link href={link.url}>
                <span className="bg-left-bottom bg-gradient-to-r from-orange-700 to-orange-200 bg-[length:0%_1px] bg-no-repeat group-hover:bg-[length:100%_1px] transition-all duration-200 ease-out font-light text-xl sm:text-lg">
                  {link.name}
                </span>
              </Link>
            </li>
          ))}
      </ul>
    </nav>
  );
}
