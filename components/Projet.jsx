import { FaGitlab } from "react-icons/fa6";
import { FaLink } from "react-icons/fa";

import {
  FaReact,
  FaHtml5,
  FaPhp,
  FaNodeJs,
  FaBootstrap,
  FaDocker,
} from "react-icons/fa";
import { RiNextjsFill } from "react-icons/ri";
import {
  SiMysql,
  SiMongodb,
  SiTailwindcss,
  SiPostman,
  SiPm2,
  SiGooglesearchconsole,
  SiVercel,
} from "react-icons/si";
import { GiJesterHat } from "react-icons/gi";
import { IoLogoJavascript, IoLogoFigma } from "react-icons/io5";
import { IoLogoWordpress } from "react-icons/io";


import Link from "next/link";

export default function Projet({ projet }) {
  const size = 30;
  const color = "#fb923c";

  const getIcon = (stack) => {
    switch (stack) {
      case "javascript":
        return (
          <IoLogoJavascript size={size} color={color} title="Javascript" />
        );
      case "react":
        return <FaReact size={size} color={color} title="ReactJS" />;
      case "nextjs":
        return <RiNextjsFill size={size} color={color} title="NextJS" />;
      case "html":
        return <FaHtml5 size={size} color={color} title="HTML" />;
      case "php":
        return <FaPhp size={size} color={color} title="PHP" />;
      case "mysql":
        return <SiMysql size={size} color={color} title="MySQL" />;
      case "mongodb":
        return <SiMongodb size={size} color={color} title="MongoDB" />;
      case "nodejs":
        return <FaNodeJs size={size} color={color} title="NodeJS" />;
      case "bootstrap":
        return <FaBootstrap size={size} color={color} title="BootStrap" />;
      case "tailwind":
        return <SiTailwindcss size={size} color={color} title="Tailwind CSS" />;
      case "jest":
        return <GiJesterHat size={size} color={color} title="Jest" />;
      case "postman":
        return <SiPostman size={size} color={color} title="PostMan" />;
      case "figma":
        return <IoLogoFigma size={size} color={color} title="Figma" />;
      case "docker":
        return <FaDocker size={size} color={color} title="Docker" />;
      case "pm2":
        return <SiPm2 size={size} color={color} title="PM2" />;
      case "seo":
        return <SiGooglesearchconsole size={size} color={color} title="SEO" />;
      case "wordpress":
        return <IoLogoWordpress size={size} color={color} title="WordPress" />;
      case "vercel":
        return <SiVercel size={size} color={color} title="Vercel" />;
      default:
        return null;
    }
  };

  return (
    <div className="flex justify-center px-6 sm:p-0">
      <div className="sm:w-3/4 h-min-80 rounded-[50px] bg-orange-100 shadow-[-20px_20px_60px_#d9cab5,_20px_-20px_60px_#fffff5] flex flex-col xl:flex-row">
        <div
          className={`${projet.imgsrc} bg-cover bg-center min-w-60 min-h-60 rounded-t-[50px] xl:rounded-l-[50px]`}
        ></div>
        <div className="p-4 flex flex-col gap-2">
          <h1 className="font-bold text-2xl text-orange-500">{projet.name}</h1>
          <h2 className="font-light text-xl text-orange-900 border-b-2 border-orange-200">
            {projet.subtitle}
          </h2>
          <span className="font-light">{projet.description}</span>
          <div className="pt-3 flex flex-col xl:flex-row items-center justify-between mr-5">
            <div className="flex flex-col min-w-fit gap-4 md:flex-row">
              {projet.buttons.map((button) => (
                <Link
                  key={button.id}
                  className="bg-orange-500 p-2 rounded-xl hover:-translate-y-1 duration-150 delay-75"
                  href={button.url}
                  target="_blank"
                >
                  <div className="flex flex-row justify-center items-center gap-4">
                    <span className="text-sm text-orange-50">
                      {button.name}
                    </span>
                    {button.type === "git" ? (
                      <FaGitlab size={15} color="#fff" />
                    ) : (
                      <FaLink size={15} color="#fff" />
                    )}
                  </div>
                </Link>
              ))}
            </div>
            <div className="flex p-4 flex-col items-center justify-center">
              <span className="text-xs text-orange-500">Stack Technique</span>
              <div className="flex flex-row gap-2">
                {projet.stack.map((stack, index) => (
                  <div key={index}>{getIcon(stack)}</div>
                ))}
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
