import {
  FaReact,
  FaHtml5,
  FaPhp,
  FaNodeJs,
  FaBootstrap,
  FaDocker,
} from "react-icons/fa";
import { RiNextjsFill } from "react-icons/ri";
import {
  SiMysql,
  SiMongodb,
  SiTailwindcss,
  SiPostman,
  SiPm2,
  SiGooglesearchconsole,
  SiCsharp,
} from "react-icons/si";
import { GiJesterHat } from "react-icons/gi";
import { IoLogoJavascript, IoLogoFigma } from "react-icons/io5";
import { IoLogoWordpress } from "react-icons/io";
import { BsDatabaseFillGear } from "react-icons/bs";

import { useState } from "react";
import Link from "next/link";

export default function SkillIcon({ logo, name, description, url }) {
  const [isHovered, setIsHovered] = useState(false);

  let icon;
  let size = 45;
  let color = "#f97316";

  switch (logo) {
    case "javascript":
      icon = <IoLogoJavascript size={size} color={color} />;
      break;
    case "react":
      icon = <FaReact size={size} color={color} />;
      break;
    case "nextjs":
      icon = <RiNextjsFill size={size} color={color} />;
      break;
    case "html":
      icon = <FaHtml5 size={size} color={color} />;
      break;
    case "php":
      icon = <FaPhp size={size} color={color} />;
      break;
    case "csharp":
      icon = <SiCsharp size={size} color={color} />;
      break;
    case "entity":
      icon = <BsDatabaseFillGear size={size} color={color} />;
      break;
    case "mysql":
      icon = <SiMysql size={size} color={color} />;
      break;
    case "mongodb":
      icon = <SiMongodb size={size} color={color} />;
      break;
    case "nodejs":
      icon = <FaNodeJs size={size} color={color} />;
      break;
    case "bootstrap":
      icon = <FaBootstrap size={size} color={color} />;
      break;
    case "tailwind":
      icon = <SiTailwindcss size={size} color={color} />;
      break;
    case "jest":
      icon = <GiJesterHat size={size} color={color} />;
      break;
    case "postman":
      icon = <SiPostman size={size} color={color} />;
      break;
    case "figma":
      icon = <IoLogoFigma size={size} color={color} />;
      break;
    case "docker":
      icon = <FaDocker size={size} color={color} />;
      break;
    case "pm2":
      icon = <SiPm2 size={size} color={color} />;
      break;
    case "seo":
      icon = <SiGooglesearchconsole size={size} color={color} />;
      break;
    case "wordpress":
      icon = <IoLogoWordpress size={size} color={color} />;
      break;

    default:
      icon = <span>Pas de logo</span>;
  }

  const content = (
    <div
      key={name}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
      className={`min-w-52 sm:min-w-36 sm:max-w-52 bg-orange-100 flex flex-row sm:flex-col gap-4 p-4 justify-center items-center rounded-xl hover:shadow-lg duration-500 ${
        isHovered ? "scale-105 bg-orange-200" : ""
      }`}
    >
      {icon}
      <h1 className="font-light">{name}</h1>
      {description ? (
        <span
          className={`text-center text-sm transition-opacity duration-500 ${
            isHovered ? " text-orange-500" : ""
          }`}
        >
          {description}
        </span>
      ) : (
        ""
      )}
    </div>
  );

  return url ? (
    <Link href={url} target="_blank">
      {content}
    </Link>
  ) : (
    <div>{content}</div>
  );
}
