/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./pages/**/*.{js,jsx}",
    "./components/**/*.{js,jsx}",
    "./app/**/*.{js,jsx}",
    "./src/**/*.{js,jsx}",
  ],
  plugins: [],
  theme: {
    extend: {
      backgroundImage: {
        hero: "url('../img/orange_bg.png')",
        barber: "url('../img/barber-shop.jpg')",
        company: "url('../img/photo-ordinateur-excel.jpg')",
        portfolio: "url('../img/portfolio.jpg')"
      },
      transitionProperty: {
        height: "height",
      },
    },
  },
};
